# Mustabot.h #

# **Mover Motores** #

## **void** avanzar(**int** potencia); ##
Inicia ambos motores en la potencia entregada.

## **void** avanzar(**int** izq, **int** der); ##
Inicia ambos motores con su potencia respectiva.

### Notas sobre la potencia de los motores ###
* Debe ser un valor entre -255 y 255.
* Los valores negativos mueven los motores en reversa.
* Si el valor es 0, los motores se detienen.

#**Colorpals**#

## **void** iniciarColorpals(); ##

Inicia ambos Colorpals. Uno debe estar conectado al pin 6 y al receptor serie 1 (pin 19) y el otro al pin 8 y al receptor serie 2 (pin 17).

## **void** leerColorpals(**int** \* colorPalValue, **int** \* colorPal2Value); ##

Lee ambos Colorpals y guarda el valor en los punteros entregados.

