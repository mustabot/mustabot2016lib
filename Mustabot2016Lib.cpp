/*
 Name:		Mustabot2016Lib.cpp
 Created:	4/23/2016 12:13:24 PM
 Author:	Mustabot 2016
*/

#define largo_promedio 10

#define mot_izq_d	12
#define mot_izq_a	11

#define mot_der_d	10
#define mot_der_a	9

//-------Colorpals--------
#define colorpal 	6
#define colorpal2 	8
#define colorpalBaud 	2400

//-------Sensores Color-----
#define sensor_color_0 	22
#define sensor_color_1 	24
#define sensor_color_2 	26
#define sensor_color_3 	28
#define sensor_color_out 	30

#define sensor_color2_0 	23
#define sensor_color2_1 	25
#define sensor_color2_2 	27
#define sensor_color2_3 	29
#define sensor_color2_out 	31

#include "Mustabot2016Lib.h"
#include "SoftwareSerial.h"

SoftwareSerial serout(255, colorpal);
SoftwareSerial serout2(255, colorpal2);

int promedio_sensor1[largo_promedio];
int promedio_sensor2[largo_promedio];
const double factor = 1.00;  //1.42	

void avanzar(int pow){
	
	avanzar(pow, pow, factor);

}

void avanzar(int pow, double f){
	avanzar(pow, pow, f);
}

void avanzar(int izq, int der){
	avanzar(izq, der, factor);
}

void avanzar(int izq, int der, double f){

	//-----Motor Izquierdo
	if (izq > 0){ //Potencia positiva
		analogWrite(mot_izq_d, izq * f);
		digitalWrite(mot_izq_a, LOW);
	}
	else if (izq < 0){	//Potecia negativa
		analogWrite(mot_izq_a, -izq * f);
		digitalWrite(mot_izq_d, LOW);
	}
	else if (izq == 0){	//Potencia 0
		digitalWrite(mot_izq_d, LOW);
		digitalWrite(mot_izq_a, LOW);
	}

	//-------Motor Derecho-------
	if (der > 0){	//Potencia positiva
		analogWrite(mot_der_a, der);
		digitalWrite(mot_der_d, LOW);
	}
	else if (der < 0){	//Potencia negativa
		analogWrite(mot_der_d, -der);
		digitalWrite(mot_der_a, LOW);
	}
	else if (der == 0){	//Potencia 0
		digitalWrite(mot_der_a, LOW);
		digitalWrite(mot_der_d, LOW);	
	}
}

void iniciarColor(){

	//--------Sensor 1----------
	pinMode(sensor_color_0, OUTPUT);  
  	pinMode(sensor_color_1, OUTPUT);  
  	pinMode(sensor_color_2, OUTPUT);  
  	pinMode(sensor_color_3, OUTPUT);  
  	pinMode(sensor_color_out, INPUT);

  	digitalWrite(sensor_color_0, HIGH); 
  	digitalWrite(sensor_color_1, HIGH);

  	//--------Sensor 2-----------
  	pinMode(sensor_color2_0, OUTPUT);  
  	pinMode(sensor_color2_1, OUTPUT);  
  	pinMode(sensor_color2_2, OUTPUT);  
  	pinMode(sensor_color2_3, OUTPUT);  
  	pinMode(sensor_color2_out, INPUT);

  	digitalWrite(sensor_color2_0, HIGH); 
  	digitalWrite(sensor_color2_1, HIGH);

}

void iniciarColorpal() {
	
	//------Reset colorpal-------
	delay(200);
	pinMode(colorpal, OUTPUT);
	digitalWrite(colorpal, LOW);
	pinMode(colorpal, INPUT);
	while (digitalRead(colorpal) != HIGH);
	pinMode(colorpal, OUTPUT);
	digitalWrite(colorpal, LOW);
	delay(80);
	pinMode(colorpal, INPUT);
	delay(200);

	//------Reset colorpal2------
	delay(200);
	pinMode(colorpal2, OUTPUT);
	digitalWrite(colorpal2, LOW);
	pinMode(colorpal2, INPUT);
	while (digitalRead(colorpal2) != HIGH);
	pinMode(colorpal2, OUTPUT);
	digitalWrite(colorpal2, LOW);
	delay(80);
	pinMode(colorpal2, INPUT);
	delay(200);

	//-----Inicializar colorpal--------
	serout.begin(colorpalBaud);
	pinMode(colorpal, OUTPUT);
	serout.print("=(00 R $ s)!"); // Loop print values, see ColorPAL documentation
	serout.end();               // Discontinue serial port for transmitting

	//-----Inicializar colorpal2--------
	serout2.begin(colorpalBaud);
	pinMode(colorpal2, OUTPUT);
	serout2.print("=(00 R $ s)!"); // Loop print values, see ColorPAL documentation
	serout2.end();               // Discontinue serial port for transmitting

	pinMode(colorpal, INPUT);
	Serial1.begin(colorpalBaud);

	pinMode(colorpal2, INPUT);
	Serial2.begin(colorpalBaud);

}

void leerColorpal(int *colorpalValue, int *colorpal2Value){
	char buffer1[4] = {'\0','\0', '\0', '\0'};
	char buffer2[4] = {'\0','\0', '\0', '\0'};

	if (Serial1.available())
	{
		buffer1[0] = Serial1.read();
		if (buffer1[0] == '$')
		{
			for (int i = 0; i < 3; ++i)
			{
				while( Serial1.available() == 0 );
				buffer1[i] = Serial1.read();
				if ( buffer1[i] == '$' ) 
					*colorpalValue = -1;
					return;
			}
			sscanf(buffer1, "%3x", colorpalValue);
		}
	}

	if (Serial2.available())
	{
		buffer2[0] = Serial2.read();
		if (buffer2[0] == '$')
		{
			for (int i = 0; i < 3; ++i)
			{
				while( Serial2.available() == 0 );
				buffer2[i] = Serial2.read();
				if ( buffer2[i] == '$' )
					*colorpal2Value = -1; 
					return;
			}
			sscanf(buffer2, "%3x", colorpal2Value);
		}
	}

	Serial.print(buffer1);
	Serial.print("\t");
	Serial.print(buffer2);
}

void leerColorpalA(int *colorpalValue, int *colorpal2Value){
	char buffer1[4] = {'\0','\0', '\0', '\0'};
	char buffer2[4] = {'\0','\0', '\0', '\0'};

	if (Serial1.available())
	{
		buffer1[0] = Serial1.read();
		if (buffer1[0] == '$')
		{
			Serial1.readBytes(buffer1, 3);	
			sscanf(buffer1, "%3x", colorpalValue);
		}
	}

	if (Serial2.available())
	{
		buffer2[0] = Serial2.read();
		if (buffer2[0] == '$')
		{
			Serial2.readBytes(buffer2, 3);
			sscanf(buffer2, "%3x", colorpal2Value);
		}
	}

	Serial.print(buffer1);
	Serial.print("\t");
	Serial.print(buffer2);
}

int leerColorpal1(int anterior){
	char buffer1[4] = {'\0','\0', '\0', '\0'};
	int value = 0;

	//---------Lectura colorpal-------------
	if (Serial1.available())
	{
		buffer1[0] = Serial1.read();
		if (buffer1[0] == '$')
		{
			for (int i = 0; i < 3; ++i)
			{
				while( Serial1.available() == 0 );
				buffer1[i] = Serial1.read();
				if ( buffer1[i] == '$' ) 
					return -1;
			}
			Serial.print(buffer1);
			Serial.print("\t");
			sscanf(buffer1, "%3x", &value);
			Serial.print(value);
			return value;
		}
	}
	else
	{
		Serial.print(buffer1);
		Serial.print("\t");
		Serial.print(anterior);
	} 
	return anterior;	
}

int leerColorpal2(int anterior){
	char buffer2[4] = {'\0','\0', '\0', '\0'};
	int value2 = 0;

	//---------Lectura colorpal2-------------
	if (Serial2.available())
	{
		buffer2[0] = Serial2.read();
		if (buffer2[0] == '$')
		{
			for (int i = 0; i < 3; ++i)
			{
				while( Serial2.available() == 0 );
				buffer2[i] = Serial2.read();
				if ( buffer2[i] == '$' ) 
					return -1;
			}
			Serial.print("\t");
			Serial.print(buffer2);
			Serial.print("\t");

			sscanf(buffer2, "%3x", &value2);
			Serial.print(value2);
			return value2;
		}
	}
	else
	{
		Serial.print("\t");
		Serial.print(buffer2);
		Serial.print("\t");
		Serial.print(anterior);
	} 
	return anterior;	
}

void leerColor(int * red, int * green, int * blue, int * red2, int * green2, int * blue2){


	//-----Leer Color 1-----
	digitalWrite(sensor_color_2, LOW);  
  	digitalWrite(sensor_color_3, LOW);  
  	//count OUT, pRed, RED  
  	*red = pulseIn(sensor_color_out, digitalRead(sensor_color_out) == HIGH ? LOW : HIGH);  
  	digitalWrite(sensor_color_3, HIGH);  
  	//count OUT, pBLUE, BLUE 
  	*blue = pulseIn(sensor_color_out, digitalRead(sensor_color_out) == HIGH ? LOW : HIGH);  
  	digitalWrite(sensor_color_2, HIGH);  
  	//count OUT, pGreen, GREEN
  	*green = pulseIn(sensor_color_out, digitalRead(sensor_color_out) == HIGH ? LOW : HIGH);

  	//-----Leer Color 2-------
	digitalWrite(sensor_color2_2, LOW);  
  	digitalWrite(sensor_color2_3, LOW);  
  	//count OUT, pRed, RED  
  	*red2 = pulseIn(sensor_color2_out, digitalRead(sensor_color2_out) == HIGH ? LOW : HIGH);  
  	digitalWrite(sensor_color2_3, HIGH);  
  	//count OUT, pBLUE, BLUE 
  	*blue2 = pulseIn(sensor_color2_out, digitalRead(sensor_color2_out) == HIGH ? LOW : HIGH);  
  	digitalWrite(sensor_color2_2, HIGH);  
  	//count OUT, pGreen, GREEN
  	*green2 = pulseIn(sensor_color2_out, digitalRead(sensor_color2_out) == HIGH ? LOW : HIGH);  
}

int obtenerPromedio(int sensor){

	int resultado = 0;

	switch(sensor){
		
		case 1:
			
			for (int i = 0; i < largo_promedio; i++)
				resultado += promedio_sensor1[i];
			
			return resultado / largo_promedio;
			break;
		
		case 2:
			
			for (int i = 0; i < largo_promedio; i++)
				resultado += promedio_sensor2[i];
			
			return resultado / largo_promedio;
			break;
		
		default:
			return -1;
	}
}

void guardarPromedio(int sensor, int valor){

	switch(sensor){

		case 1:
			for (int i = largo_promedio - 1; i > 0; i--)
				promedio_sensor1[i] = promedio_sensor1[i - 1];
			
			promedio_sensor1[0] = valor;
			break;
		
		case 2:
			for (int i = largo_promedio - 1; i > 0; i--)
				promedio_sensor2[i] = promedio_sensor2[i - 1];
			
			promedio_sensor2[0] = valor;
			break;
		
		default:
			break;
	}
	return;
}

float promediar(int data[], int largo){
	float resultado = 0;

	for (int i = 0; i < largo; i++){
		resultado += data[i];
	}

	return (resultado/largo);

}
