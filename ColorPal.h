/*
 Name:		ColorPal.h
 Created:	4/23/2016 12:13:24 PM
 Author:	Mustabot 2016
*/
#ifndef _ColorPal_h
#define _ColorPal_h

#include "Arduino.h"

class ColorPal
{
	public:
		ColorPal(int pin, Stream &s);
		int read();
		bool readMulti(int *red, int *green, int *blue);
		int begin();
		int beginMulti();
		int obtenerPromedio();
		void guardarPromedio(int valor);
	private:
		Stream *ColorPalStream;
		int _pin;
		int promedio[5];
};

#endif

