/*
 Name:		Mustabot2016Lib.h
 Created:	4/23/2016 12:13:24 PM
 Author:	Mustabot 2016
*/

#ifndef _Mustabot2016Lib_h
#define _Mustabot2016Lib_h

 #include "Arduino.h"

void iniciarColor();
void iniciarColorpal();
void leerColorpal(int *sioValue, int * sio2Value);
void leerColorpalA(int *sioValue, int * sio2Value);
int leerColorpal1(int anterior);
int leerColorpal2(int anterior);
void leerColor(int * red, int * green, int * blue, int * red2, int * green2, int * blue2);
void avanzar(int izq, int der, double f);
void avanzar(int pow);
void avanzar(int pow, double f);
void avanzar(int izq, int der);
int obtenerPromedio(int sensor);
void guardarPromedio(int sensor, int valor);
float promediar(int data[], int largo);

#endif

