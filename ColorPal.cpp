/*
 Name:		ColorPal.cpp
 Created:	4/23/2016 12:13:24 PM
 Author:	Mustabot 2016
*/

#include "ColorPal.h"
#include "SoftwareSerial.h"

#define colorPalBaud 4800

 ColorPal::ColorPal(int pin, Stream &s){
 	_pin = pin;
 	ColorPalStream = &s;
 }

 int ColorPal::begin(){

 	SoftwareSerial serout(255, _pin);

 	delay(200);
 	pinMode(_pin, OUTPUT);
 	digitalWrite(_pin , LOW);
 	pinMode(_pin, INPUT);
 	unsigned long start = millis();
 	while (digitalRead(_pin) != HIGH){
 		if (millis() - start >= 100)
 			return -1;
 	}
 	pinMode(_pin, OUTPUT);
 	digitalWrite(_pin, LOW);
 	delay(80);
 	pinMode(_pin, INPUT);
 	delay(200);

 	serout.begin(colorPalBaud);
 	pinMode(_pin, OUTPUT);
 	serout.print("=(00 Y $ s)!");
 	serout.end();

 	pinMode(_pin, INPUT);

 	return 0;

 }

 int ColorPal::beginMulti(){

 	SoftwareSerial serout(255, _pin);

 	delay(200);
 	pinMode(_pin, OUTPUT);
 	digitalWrite(_pin, LOW);
 	pinMode(_pin, INPUT);
 	unsigned long start = millis();
 	while (digitalRead(_pin) != HIGH){
 		if (millis() - start >= 100)
 			return -1;
 	}
 	pinMode(_pin, OUTPUT);
 	digitalWrite(_pin, LOW);
 	delay(80);
 	pinMode(_pin, INPUT);
 	delay(200);

 	serout.begin(colorPalBaud);
 	pinMode(_pin, OUTPUT);
 	serout.print("=(00 $ m)!");
 	serout.end();

 	pinMode(_pin, INPUT);

 	return 0;

 }

 bool ColorPal::readMulti(int *red, int *green, int* blue){

 	char buffer[32];

 	for (int i = 0; i < 32; i++){
 		buffer[i] = '\0';
 	}


 	if (ColorPalStream->available() > 0) {
    // Wait for a $ character, then read three 3 digit hex numbers
 		buffer[0] = ColorPalStream->read();
 		if (buffer[0] == '$') {
 			for(int i = 0; i < 9; i++) {
        		while (ColorPalStream->available() == 0);     // Wait for next input character
        		buffer[i] = ColorPalStream->read();
        		if (buffer[i] == '$')               // Return early if $ character encountered
        			return false;
        	}
        	sscanf(buffer, "%3x%3x%3x", &red, &green, &blue);
        	return true;
        }
    } else return false;


	/*//---------Lectura colorpal-------------
    if (ColorPalStream->available() > 0)
    {
    	buffer[0] = ColorPalStream->read();
    	if (buffer[0] == '$')
    	{
    		for (int i = 0; i < 9; ++i)
    		{
    			while( ColorPalStream->available() == 0 );
    			buffer[i] = ColorPalStream->read();
    			if ( buffer[i] == '$' ) 
    				return false;
    		}
    		sscanf(buffer, "%3x%3x%3x", &red, &green, &blue);
    		return true;
    	}
    }else return false;*/


}

int ColorPal::read(){

	char buffer1[4] = {'\0','\0', '\0', '\0'};
	int value = 0;

	//---------Lectura colorpal-------------
	if (ColorPalStream->available())
	{
		buffer1[0] = ColorPalStream->read();
		if (buffer1[0] == '$')
		{
			for (int i = 0; i < 3; ++i)
			{
				while( ColorPalStream->available() == 0 );
				buffer1[i] = ColorPalStream->read();
				if ( buffer1[i] == '$' ) 
					return -1;
			}
			sscanf(buffer1, "%3x", &value);
			return value;
		}
	}else return -1;
}